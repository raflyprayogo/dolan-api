<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Library API EMAIL CLIENT
*/
class CSMail 
{
	protected $CI;

	public $server 		= 'http://139.59.240.112/apiemail/';
	public $key 		= 'emailAPI@ericariyanto';
	public $host 		= 'smtp.gmail.com';
	public $port 		= '587';
	public $smtpsecure 	= 'tls';

	public $username 	= 'vistostudio@gmail.com';
	public $password 	= 'maknacreative';
	public $sendername 	= 'CHS (do not reply)';

	public function __construct()
	{
		$this->CI =& get_instance(); 
	}

	public function send($email, $config = null)
	{
		$_config 	= $this->_get_config();

		if ( !is_null($config) ) : 
			foreach ($config as $key => $value) {
				if (isset($_config[$key])){
					$_config[$key] 	= $value;
				}
			}
		endif;

		// generate param
		$param 		= $this->_generate_param( $email, $_config );

		// set url 
		$url 		= $this->server . 'services/mailsend/' . $param;
		
		// get response
		$response 	= $this->_get_content_from_url( $url );

		return $response;

	}

	private function _get_web_page( $url )
    {
        $user_agent='Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';

        $options = array(

            CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
            CURLOPT_POST           =>false,        //set to GET
            CURLOPT_USERAGENT      => $user_agent, //set user agent
            CURLOPT_COOKIEFILE     =>"cookie.txt", //set cookie file
            CURLOPT_COOKIEJAR      =>"cookie.txt", //set cookie jar
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
        );

        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;
        return $header;
    }

    private function _get_content_from_url($url) {
    	//Read a web page and check for errors:
    	$str 	= "";
		$result = $this->_get_web_page( $url );

		if ( $result['errno'] != 0 )		    
			$str = "error: bad url, timeout, redirect loop";

		else if ( $result['http_code'] != 200 )
			$str 	= "error: no page, no permissions, no service";		    
		else
			$str = $result['content'];

		return $str;
    }

	private function _generate_param($email, $config)
	{
		$param 	= ['email' => $email, 'config' => $config];
		$json 	= json_encode($param);		
		$url 	= $this->_CSEncrypt($json);

		return $url;
	}

	private function _CSEncrypt($str='', $key = '')
    {        
        //sorry script hidden, please email your request to eric.ariyanto@gmail.com
    }    

	private function _get_config()
	{
		$config 	= [
			'Host' 			=> $this->host,    			
			'SetFrom' 		=> [$this->username, $this->sendername],
			'SMTPSecure' 	=> $this->smtpsecure,    			
			'SMTPAuth' 		=> true,
			'Port' 			=> $this->port,
			'Username' 		=> $this->username,
			'Password' 		=> $this->password,
			'FromName' 		=> $this->sendername
		];

		return $config;
	}



}