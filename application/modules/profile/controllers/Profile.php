<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->headers          = $this->input->request_headers();
        $this->api_version      = $this->headers['Api-Version'];
        date_default_timezone_set('UTC');
        header('Content-Type: application/json');
        acc_token();
    }

    public function index(){   
        echo "Rokkap Profile Rest API";
    }

    function get_user_profile(){
        if($this->api_version == '1'){
            $my_id              =  $this->headers['User-Id'];
            $my_uname           =  $this->headers['User-Name'];
            $user_id            = $this->input->post('user_id');  

            $join_profile       = [
                                    ['table' => 'user_father uf', 'on' => 'uf.userfather_user_id=user_id', 'tipe' => 'LEFT'],
                                    ['table' => 'user_mother um', 'on' => 'um.usermother_user_id=user_id', 'tipe' => 'LEFT'],
                                    ['table' => 'clan ucf', 'on' => 'uf.userfather_clan_id=ucf.clan_id', 'tipe' => 'LEFT'],
                                    ['table' => 'clan ucm', 'on' => 'um.usermother_clan_id=ucm.clan_id', 'tipe' => 'LEFT'],
                                ];
            $get_profile        = $this->m_global->get_data_all('user', $join_profile, ['user_id' => $user_id], "user_id, user_username, CONCAT_WS(' ', user_firstname , user_lastname ) as user_fullname, user_avatar, user_gender, user_birthdate, user_bio, ucf.clan_name as father_clan, ucm.clan_name as mother_clan, user_isverified, user_address, user_birthplace, user_address_lat, user_address_long");

            $join_photos        = [
                                    ['table' => 'feed_image', 'on' => 'feedimage_feed_id=feed_id']
                                ];
            $get_feed           = $this->m_global->get_data_all('feed', $join_photos, ['feed_user_id' => $user_id , 'feed_status' => '1'], "feed_id,feed_createddate", null, ['feed_createddate', 'DESC'], 0, 6, ['feed_id']);
            $photos             = [];
            for ($i=0; $i < count($get_feed); $i++) { 
                $get_photos     = $this->m_global->get_data_all('feed_image', null, ['feedimage_feed_id' => $get_feed[$i]->feed_id], 'feedimage_link');
                $photos[$i]['id']           = $get_feed[$i]->feed_id;   
                for ($p=0; $p < count($get_photos); $p++) { 
                    $photos[$i]['list'][$p] = $get_photos[$p]->feedimage_link;
                }
            }
            $get_profile[0]->age            = (String)get_age($get_profile[0]->user_birthdate);
            $get_profile[0]->father_clan    = ucfirst(strtolower($get_profile[0]->father_clan));
            $get_profile[0]->mother_clan    = ucfirst(strtolower($get_profile[0]->mother_clan));
            $get_profile[0]->verified       = ($get_profile[0]->user_isverified == 1 ? true : false);

            $get_personality        = $this->m_global->get_data_all('user_personality', [['table' => 'personality', 'on' => 'userperson_personality_id=personality_id']], ['userperson_user_id' => $user_id], 'personality_name');
            $data['personality']    = [];
            for ($i=0; $i < count($get_personality); $i++) { 
                $data['personality'][$i] = $get_personality[$i]->personality_name;
            }

            $get_hobby              = $this->m_global->get_data_all('user_hobby', [['table' => 'hobby', 'on' => 'userhobb_hobby_id=hobby_id']], ['userhobb_user_id' => $user_id], 'hobby_name');
            $data['hobby']          = [];
            for ($i=0; $i < count($get_hobby); $i++) { 
                $data['hobby'][$i]  = $get_hobby[$i]->hobby_name;
            }

            $check_isloved          = $this->m_global->count_data_all('love', null, ['love_target_id' => $user_id, 'love_user_id' => $my_id , 'love_status' => '1']);

            $data['profile']        = $get_profile[0];
            $data['photos']         = $photos;
            $data['total_post']     = $this->m_global->count_data_all('feed', null, ['feed_user_id' => $user_id, 'feed_status' => '1']);
            $data['total_friends']  = $this->m_global->count_data_all('friend', null, ['friend_user_id' => $user_id, 'friend_status' => '2']);
            $data['total_love']     = $this->m_global->count_data_all('love', null, ['love_target_id' => $user_id, 'love_status' => '1']);
            $data['socmed']         = $this->m_global->get_data_all('user_socmed', null, ['usersoc_user_id' => $user_id], 'usersoc_facebook, usersoc_ig, usersoc_linkedin, usersoc_google');
            $data['is_loved']       = ($check_isloved > 0 ? true : false );

            $check_friend_status    = $this->m_global->get_data_all('friend', null, ['friend_user_id' => $my_id, 'friend_target_id' => $user_id]);
            if(count($check_friend_status) > 0){
                $fstat              = $check_friend_status[0]->friend_status;
                if($fstat == '1'){
                    $data['friend_status']       = "1";
                    $data['friend_status_info']  = "waiting for confirmation";
                }else if($fstat == '2'){
                    $data['friend_status']       = "2";
                    $data['friend_status_info']  = "already friends";
                }else{
                    $data['friend_status']       = "3";
                    $data['friend_status_info']  = "friendship rejected";
                }
                $data['friend_chat_room']    = ($check_friend_status[0]->friend_chat_room != '' ? $check_friend_status[0]->friend_chat_room : '');
            }else{
                $check_friend_status             = $this->m_global->get_data_all('friend', null, ['friend_user_id' => $user_id, 'friend_target_id' => $my_id]);
                if(count($check_friend_status) > 0){
                    $fstat              = $check_friend_status[0]->friend_status;
                    if($fstat == '1'){
                        $data['friend_status']       = "11";
                        $data['friend_status_info']  = "waiting for your confirmation";
                    }else if($fstat == '2'){
                        $data['friend_status']       = "2";
                        $data['friend_status_info']  = "already friends";
                        $data['friend_chat_room']    = ($check_friend_status[0]->friend_chat_room != '' ? $check_friend_status[0]->friend_chat_room : '');
                    }else{
                        $data['friend_status']       = "3";
                        $data['friend_status_info']  = "friendship rejected";
                    }
                    $data['friend_chat_room']       = ($check_friend_status[0]->friend_chat_room != '' ? $check_friend_status[0]->friend_chat_room : '');
                }else{
                    $data['friend_status']          = "0";
                    $data['friend_status_info']     = "not yet friends";
                    $data['friend_chat_room']       = "";
                }
            }

            if($my_id != $user_id){
                $get_visit  = $this->m_global->get_data_all('visit', null, ['visit_user_id' => $my_id, 'visit_target_id' => $user_id]);
                if(count($get_visit) == 0){
                    $dvisit['visit_user_id']        = $my_id;
                    $dvisit['visit_target_id']      = $user_id;
                    $dvisit['visit_createddate']    = date('Y-m-d H:i:s');
                    $result_visit = $this->m_global->insert('visit',$dvisit);
                    if($result_visit['status']){
                        insert_notif_list($user_id, $my_id, 'see_profile', $result_visit['id']);
                        send_notification($user_id, $my_uname, 'Melihat profil anda', 'see_profile', '{"user_id":'.$my_id.'}');
                    }
                }
            }

            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_user_id_by_username(){
        if($this->api_version == '1'){
            $username           = $this->input->post('username');
            $data               = $this->m_global->get_data_all('user', null, ['user_username' => $username], "user_id");
            if(count($data) > 0){
                echo response_builder(true, 200, $data[0]->user_id);
            }else{
                echo response_builder(false, 404);
            }
            
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_user_photos(){
        if($this->api_version == '1'){
            $my_id      =  $this->headers['User-Id'];
            $user_id    = $this->input->post('user_id'); 

            $join_photos = [
                                ['table' => 'feed_image', 'on' => 'feedimage_feed_id=feed_id']
                            ];
            $get_feed   = $this->m_global->get_data_all('feed', $join_photos, ['feed_user_id' => $user_id , 'feed_status' => '1'], "feed_id,feed_createddate", null, ['feed_createddate', 'DESC'], 0, null, ['feed_id']);
            $photos     = [];
            for ($i=0; $i < count($get_feed); $i++) { 
                $get_photos     = $this->m_global->get_data_all('feed_image', null, ['feedimage_feed_id' => $get_feed[$i]->feed_id], 'feedimage_link');
                $photos[$i]['id']           = $get_feed[$i]->feed_id;   
                for ($p=0; $p < count($get_photos); $p++) { 
                    $photos[$i]['list'][$p] = $get_photos[$p]->feedimage_link;
                }
            }

            echo response_builder(true, 200, $photos);
        }else{
            echo response_builder(false, 900);
        }
    }

    function update_profile(){
        if($this->api_version == '1'){
            $username       = $this->input->post('username');
            $first_name     = $this->input->post('first_name');
            $last_name      = $this->input->post('last_name');
            $phone          = $this->input->post('phone');
            $birthdate      = $this->input->post('birthdate');
            $gender         = $this->input->post('gender');
            $avatar         = $this->input->post('avatar');
            $father_clan    = $this->input->post('father_clan');
            $mother_clan    = $this->input->post('mother_clan');

            $user_id        = $this->headers['User-Id'];
            $token          = $this->headers['Api-Token'];

            $check_uname    = $this->m_global->count_data_all('user',null,['user_username' => $username, 'user_id <>' => $user_id]);
            if($check_uname > 0){
                echo response_builder(false, 403, null, 'username already used');
                exit;
            }

            if($avatar != ''){
                $data['user_avatar']    = $avatar;
            }

            $data['user_username']      = $username;
            $data['user_firstname']     = $first_name;
            $data['user_lastname']      = $last_name;
            $data['user_phone']         = $phone;
            $data['user_birthdate']     = $birthdate;
            $data['user_gender']        = $gender;

            if($this->input->post('step1') == '1'){
                $data['user_bio']               = $this->input->post('bio');
                $data['user_birthplace']        = $this->input->post('birthplace');
                $data['user_address_lat']       = $this->input->post('addr_lat');
                $data['user_address_long']      = $this->input->post('addr_long');
                $data['user_address']           = $this->input->post('address');
            }

            $result                     = $this->m_global->update('user', $data, ['user_id' => $user_id]);
            if($result){
                // update clan 1
                $dataclan1['userfather_clan_id']    = $this->check_clan($father_clan, $user_id);
                $res_clan1                          = $this->m_global->update('user_father', $dataclan1, ['userfather_user_id' => $user_id]);

                // update clan 2
                $dataclan2['usermother_clan_id']    = $this->check_clan($mother_clan, $user_id);
                $res_clan2                          = $this->m_global->update('user_mother', $dataclan2, ['usermother_user_id' => $user_id]);

                //if step 1
                if($this->input->post('step1') == '1'){
                    $hobb_arr       = json_decode($this->input->post('hobby'), true);
                    $this->m_global->delete('user_hobby', ['userhobb_user_id' => $user_id]);
                    for ($i=0; $i < count($hobb_arr); $i++) { 
                        $dhobb['userhobb_hobby_id']      = $hobb_arr[$i]['data'];
                        $dhobb['userhobb_user_id']       = $user_id;
                        $this->m_global->insert('user_hobby', $dhobb);
                    }

                    $person_arr       = json_decode($this->input->post('personality'), true);
                    $this->m_global->delete('user_personality', ['userperson_user_id' => $user_id]);
                    for ($i=0; $i < count($person_arr); $i++) { 
                        $dpers['userperson_personality_id']     = $person_arr[$i]['data'];
                        $dpers['userperson_user_id']            = $user_id;
                        $this->m_global->insert('user_personality', $dpers);
                    }
                }

                // if step 3
                if($this->input->post('step3') == '1'){
                    $datasoc['usersoc_facebook']   = $this->input->post('fb');
                    $datasoc['usersoc_ig']         = $this->input->post('ig');
                    $datasoc['usersoc_linkedin']   = $this->input->post('linkedin');
                    $datasoc['usersoc_google']     = $this->input->post('google');
                    $this->m_global->update('user_socmed', $datasoc, ['usersoc_user_id' => $user_id]);
                }

                // data return 
                $get_data                   = $this->m_global->get_data_all('user', null, ['user_id' => $user_id])[0];
                $join_profile               = [
                                                ['table' => 'user_father uf', 'on' => 'uf.userfather_user_id=user_id', 'tipe' => 'LEFT'],
                                                ['table' => 'user_mother um', 'on' => 'um.usermother_user_id=user_id', 'tipe' => 'LEFT'],
                                                ['table' => 'clan ucf', 'on' => 'uf.userfather_clan_id=ucf.clan_id', 'tipe' => 'LEFT'],
                                                ['table' => 'clan ucm', 'on' => 'um.usermother_clan_id=ucm.clan_id', 'tipe' => 'LEFT'],
                                            ];
                $get_clan                   = $this->m_global->get_data_all('user', $join_profile, ['user_id' => $user_id], 'ucf.clan_name as father_clan, ucm.clan_name as mother_clan')[0];
                $get_data->father_clan      = $get_clan->father_clan;
                $get_data->mother_clan      = $get_clan->mother_clan;
                $get_data->user_fullname    = $get_data->user_firstname.' '.$get_data->user_lastname;
                $get_data->api_token        = $token;
                echo response_builder(true, 201, $get_data);
            }else{
                echo response_builder(false, 406, null, 'failed update data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function update_socmed(){
        if($this->api_version == '1'){
            $fb             = $this->input->post('fb');
            $ig             = $this->input->post('ig');
            $linkedin       = $this->input->post('linkedin');
            $google         = $this->input->post('google');
            $user_id        = $this->headers['User-Id'];

            $data['usersoc_facebook']   = $fb;
            $data['usersoc_ig']         = $ig;
            $data['usersoc_linkedin']   = $linkedin;
            $data['usersoc_google']     = $google;

            $check_data     = $this->m_global->get_data_all('user_socmed', null, ['usersoc_user_id' => $user_id]);
            if(count($check_data) > 0){
                $result     = $this->m_global->update('user_socmed', $data, ['usersoc_id' => $check_data[0]->usersoc_id]);
                if($result){
                    echo response_builder(true, 201);
                }else{
                    echo response_builder(false, 406, null, 'failed update data');
                }
            }else{
                $data['usersoc_user_id']   = $user_id;
                $result     = $this->m_global->insert('user_socmed', $data);
                if($result['status']){
                    echo response_builder(true, 201);
                }else{
                    echo response_builder(false, 406, null, 'failed create data');
                }
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function post_love(){
        if($this->api_version == '1'){
            $status                     = $this->input->post('status');
            $target_id                  = $this->input->post('target_id');
            $user_id                    = $this->headers['User-Id'];
            $user_name                  = $this->headers['User-Name'];

            $data['love_status']        = $status;

            $check_data     = $this->m_global->get_data_all('love', null, ['love_user_id' => $user_id, 'love_target_id' => $target_id]);
            if(count($check_data) > 0){
                $result     = $this->m_global->update('love', $data, ['love_user_id' => $user_id, 'love_target_id' => $target_id]);
                if($result){
                    echo response_builder(true, 201);
                }else{
                    echo response_builder(false, 406, null, 'failed update data');
                }
            }else{
                $data['love_user_id']       = $user_id;
                $data['love_target_id']     = $target_id;
                $data['love_createddate']   = date('Y-m-d H:i:s');

                $result     = $this->m_global->insert('love', $data);
                if($result['status']){
                    insert_notif_list($target_id, $user_id, 'love_profile', $result['id']);
                    send_notification($target_id, $user_name, 'Menyukai profil anda', 'love_profile', '{"user_id":'.$user_id.'}');
                    echo response_builder(true, 201);
                }else{
                    echo response_builder(false, 406, null, 'failed create data');
                }
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function post_clan(){
        if($this->api_version == '1'){
            $name           = $this->input->post('name');
            $user_id        = $this->headers['User-Id'];

            $check_data     = $this->m_global->get_data_all('clan', null, ['clan_name' => $name]);
            if(count($check_data) == 0){
                $data['clan_name']          = $name;
                $data['clan_created_by']    = $user_id;
                $data['clan_createddate']   = date('Y-m-d H:i:s');

                $result     = $this->m_global->insert('clan', $data);
                if($result['status']){
                    $rdata['id'] = $result['id'];
                    echo response_builder(true, 201, $rdata);
                }else{
                    echo response_builder(false, 406, null, 'failed create data');
                }
            }else{
                $rdata['id'] = $check_data[0]->clan_id;
                echo response_builder(true, 409, $rdata, $name.' already exist');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_personality(){
        if($this->api_version == '1'){
            $data               = $this->m_global->get_data_all('personality', null, ['personality_status' => '1'], 'personality_id,personality_name');
            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_hobby(){
        if($this->api_version == '1'){
            $data               = $this->m_global->get_data_all('hobby', null, ['hobby_status' => '1'], 'hobby_id,hobby_name');
            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function check_complete_profile(){
        if($this->api_version == '1'){
            $user_id            = $this->input->post('user_id');

            $check_user         = $this->m_global->get_data_all('user', null, ['user_id' => $user_id])[0];
            $check_personality  = $this->m_global->count_data_all('user_personality', null, ['userperson_user_id' => $user_id]);
            $check_hobby        = $this->m_global->count_data_all('user_hobby', null, ['userhobb_user_id' => $user_id]);
            $check_socmed       = $this->m_global->count_data_all('user_socmed', null, ['usersoc_user_id' => $user_id]);

            if($check_user->user_address != '' && $check_personality > 0 && $check_hobby > 0){
                $data['step_1'] = true;
            }else{
                $data['step_1'] = false;
            }

            if($check_user->user_idcard_number != '' && $check_user->user_idcard_image != '' && $check_user->user_idcard_selfie != ''){
                $data['step_2'] = true;
            }else{
                $data['step_2'] = false;
            }

            if($check_socmed > 0){
                $data['step_3'] = true;
            }else{
                $data['step_3'] = false;
            }

            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function post_complete_profile(){
        if($this->api_version == '1'){
            $bio            = $this->input->post('bio');
            $birthplace     = $this->input->post('birthplace');
            $addr_lat       = $this->input->post('addr_lat');
            $addr_long      = $this->input->post('addr_long');
            $address        = $this->input->post('address');
            $hobby          = $this->input->post('hobby');
            $personality    = $this->input->post('personality');
            $user_id        = $this->headers['User-Id'];

            $data['user_bio']           = $bio;
            $data['user_address']       = $address;
            $data['user_address_lat']   = $addr_lat;
            $data['user_address_long']  = $addr_long;
            $data['user_birthplace']    = $birthplace;

            $result     = $this->m_global->update('user', $data, ['user_id' => $user_id]);
            if($result){
                $hobb_arr       = json_decode($hobby, true);
                for ($i=0; $i < count($hobb_arr); $i++) { 
                    $dhobb['userhobb_hobby_id']      = $hobb_arr[$i]['data'];
                    $dhobb['userhobb_user_id']       = $user_id;
                    $this->m_global->insert('user_hobby', $dhobb);
                }

                $person_arr       = json_decode($personality, true);
                for ($i=0; $i < count($person_arr); $i++) { 
                    $dpers['userperson_personality_id']     = $person_arr[$i]['data'];
                    $dpers['userperson_user_id']            = $user_id;
                    $this->m_global->insert('user_personality', $dpers);
                }

                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed update data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function post_update_ktp(){
        if($this->api_version == '1'){
            $number         = $this->input->post('number');
            $idcard         = $this->input->post('idcard');
            $selfie         = $this->input->post('selfie');
            $user_id        = $this->headers['User-Id'];

            $data['user_idcard_number']   = $number;
            $data['user_idcard_image']    = $idcard;
            $data['user_idcard_selfie']   = $selfie;

            $result     = $this->m_global->update('user', $data, ['user_id' => $user_id]);
            if($result){
                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed update data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function post_chat_room(){
        if($this->api_version == '1'){
            $room           = $this->input->post('room');
            $target_id      = $this->input->post('target_id');
            $user_id        = $this->headers['User-Id'];

            $data['friend_chat_room']   = $room;

            $result1        = $this->m_global->update('friend', $data, ['friend_user_id' => $user_id, 'friend_target_id' => $target_id]);
            $result2        = $this->m_global->update('friend', $data, ['friend_user_id' => $target_id, 'friend_target_id' => $user_id]);
            if($result1 && $result2){
                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed update data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_my_friend(){
        if($this->api_version == '1'){
            $user_id    = $this->headers['User-Id'];
            $page       = $this->input->post('page');

            $limit      = 20;
            $offset     = $limit*$page;
            $join       = [
                            ['table' => 'user', 'on' => 'friend_target_id=user_id']
                        ];
            $data       = $this->m_global->get_data_all('friend', $join, ['friend_user_id' => $user_id, 'friend_status' => '2'], "CONCAT_WS(' ', user_firstname , user_lastname ) as user_fullname, user_avatar, user_id", null,['friend_createddate', 'DESC'],$offset, $limit);
            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_request_friend(){
        if($this->api_version == '1'){
            $user_id    = $this->headers['User-Id'];
            $page       = $this->input->post('page');

            $limit      = 20;
            $offset     = $limit*$page;
            $join       = [
                            ['table' => 'user', 'on' => 'friend_user_id=user_id']
                        ];
            $data       = $this->m_global->get_data_all('friend', $join, ['friend_target_id' => $user_id, 'friend_status' => '1'], "CONCAT_WS(' ', user_firstname , user_lastname ) as user_fullname, user_avatar, user_id", null,['friend_createddate', 'DESC'],$offset, $limit);
            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_friend_dashboard(){
        if($this->api_version == '1'){
            $user_id        = $this->headers['User-Id'];
            $user_gender    = $this->input->post('gender');
            $user_clan_f    = $this->input->post('clan_f');
            $user_clan_m    = $this->input->post('clan_m');

            $join       = [
                            ['table' => 'user', 'on' => 'friend_target_id=user_id']
                        ];

            $join_req   = [
                            ['table' => 'user', 'on' => 'friend_user_id=user_id']
                        ];

            $check_score            = $this->m_global->get_data_all('score', null,['score_user_id' => $user_id]);
            if(count($check_score) > 0){
                $data['quiz_submitted']     = true;
                $data['sugest']             = $this->get_suggest($user_id,$user_gender, $user_clan_f, $user_clan_m, $check_score[0]->score_category);
            }else{
                $data['quiz_submitted']     = false;
                $data['sugest']             = [];
            }

            $data['request']        = $this->m_global->get_data_all('friend', $join_req, ['friend_target_id' => $user_id, 'friend_status' => '1'], "CONCAT_WS(' ', user_firstname , user_lastname ) as user_fullname, user_avatar, user_id", null,['friend_createddate', 'DESC'],0, 10);
            $data['newest']         = $this->m_global->get_data_all('friend', $join, ['friend_user_id' => $user_id, 'friend_status' => '2'], "CONCAT_WS(' ', user_firstname , user_lastname ) as user_fullname, user_avatar, user_id", null,['friend_createddate', 'DESC'],0, 10);
            $data['total_friends']  = $this->m_global->count_data_all('friend', null, ['friend_user_id' => $user_id, 'friend_status' => '2']);
            $data['total_request']  = $this->m_global->count_data_all('friend', null, ['friend_target_id' => $user_id, 'friend_status' => '1']);
            
            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_suggest($user_id, $gender, $clan_f, $clan_m, $category){
        $join_profile       = [
                                    ['table' => 'user_father uf', 'on' => 'uf.userfather_user_id=user_id', 'tipe' => 'LEFT'],
                                    ['table' => 'user_mother um', 'on' => 'um.usermother_user_id=user_id', 'tipe' => 'LEFT'],
                                    ['table' => 'clan ucf', 'on' => 'uf.userfather_clan_id=ucf.clan_id', 'tipe' => 'LEFT'],
                                    ['table' => 'clan ucm', 'on' => 'um.usermother_clan_id=ucm.clan_id', 'tipe' => 'LEFT'],
                                    ['table' => 'score', 'on' => 'score_user_id=user_id']
                                ];
        $select             = "user_id, CONCAT_WS(' ', user_firstname , user_lastname ) as user_fullname, user_avatar, ucf.clan_name as father_clan, ucm.clan_name as mother_clan";        
        if($gender == "M"){
            $where          = ['user_gender' => 'F', 'ucf.clan_name' => $clan_m, 'score_category' => $category];
        }else{
            $where          = ['user_gender' => 'M', 'ucm.clan_name' => $clan_f, 'score_category' => $category];
        }
        $result             = [];
        $get_user           = $this->m_global->get_data_all('user', $join_profile, $where, $select);
        for ($i=0; $i < count($get_user); $i++) {
            $get_user[$i]->father_clan      = ucfirst(strtolower($get_user[$i]->father_clan));
            $get_user[$i]->mother_clan      = ucfirst(strtolower($get_user[$i]->mother_clan));
            $check_friend_status            = $this->m_global->count_data_all('friend', null, ['friend_user_id' => $user_id, 'friend_target_id' => $get_user[$i]->user_id]);
            if($check_friend_status == 0){
                $result[]   = $get_user[$i];
            }
        }
        return $result;
    }

    function add_friend(){
        if($this->api_version == '1'){
            $user_id    = $this->headers['User-Id'];
            $user_name  = $this->headers['User-Name'];
            $target_id  = $this->input->post('target_id');

            $get_verified   = $this->m_global->get_data_all('user', null, ['user_id' => $user_id], 'user_isverified')[0];
            if($get_verified->user_isverified == '1'){
                $data['friend_user_id']     = $user_id;
                $data['friend_target_id']   = $target_id;
                $data['friend_status']      = '1';
                $data['friend_createddate'] = date('Y-m-d H:i:s');

                $result     = $this->m_global->insert('friend', $data);
                if($result['status']){
                    insert_notif_list($target_id, $user_id, 'friend_request', $result['id']);
                    send_notification($target_id, $user_name, 'Meminta anda untuk berteman', 'add_friend', '{}');
                    echo response_builder(true, 201);
                }else{
                    echo response_builder(false, 406, null, 'failed insert data');
                }
            }else{
                echo response_builder(false, 412, null, 'not verified');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function accept_friend(){
        if($this->api_version == '1'){
            $user_id    = $this->headers['User-Id'];
            $user_name  = $this->headers['User-Name'];
            $target_id  = $this->input->post('target_id');

            $data['friend_user_id']     = $user_id;
            $data['friend_target_id']   = $target_id;
            $data['friend_status']      = '2';
            $data['friend_createddate'] = date('Y-m-d H:i:s');

            $result     = $this->m_global->insert('friend', $data);
            if($result['status']){

                $dataupd['friend_status'] = '2';
                $this->m_global->update('friend', $dataupd, ['friend_user_id' => $target_id, 'friend_target_id' => $user_id]);

                insert_notif_list($target_id, $user_id, 'friend_accept', $result['id']);
                send_notification($target_id, $user_name, 'Menerima permintaan pertemanan anda', 'approve_friend', '{"user_id":'.$user_id.'}');

                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed insert data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_notification(){
        if($this->api_version == '1'){
            $user_id    = $this->headers['User-Id'];
            // $user_id    = '2';
            $page       = $this->input->post('page');

            $limit      = 20;
            $offset     = $limit*$page;
            $join       = [
                            ['table' => 'user', 'on' => 'notif_sender_id=user_id']
                        ];
            $join_feed  = [
                            ['table' => 'user', 'on' => 'feed_user_id=user_id']
                        ];
            $data       = $this->m_global->get_data_all('notification', $join, ['notif_target_id' => $user_id], "CONCAT_WS(' ', user_firstname , user_lastname ) as sender_fullname, user_avatar as sender_avatar, user_id as sender_id ,notif_type,notif_object_id, notif_createddate", null,['notif_createddate', 'DESC'],$offset, $limit);
            
            for ($i=0; $i < count($data); $i++) { 
                if($data[$i]->notif_type == 'comment'){
                    $get_com = $this->m_global->get_data_all('feed_comment', null, ['feedcom_id' => $data[$i]->notif_object_id]);
                    if(count($get_com) > 0){
                        $data[$i]->notif_object_desc                = $get_com[0]->feedcom_content;
                        $data[$i]->notif_parent_object_type         = 'feed';
                        $data[$i]->notif_parent_object_id           = $get_com[0]->feedcom_feed_id;

                        $f_info                                     = $this->m_global->get_data_all('feed', $join_feed, ['feed_id' => $get_com[0]->feedcom_feed_id],"feed_user_id, CONCAT_WS(' ', user_firstname , user_lastname ) as user_fullname")[0];
                        $data[$i]->notif_parent_object_owner_id     = $f_info->feed_user_id;
                        $data[$i]->notif_parent_object_owner_name   = $f_info->user_fullname;
                    }
                    
                }
                if($data[$i]->sender_avatar == null){
                    $data[$i]->sender_avatar = '-';
                }
            }

            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function update_password(){
        if($this->api_version == '1'){
            $user_id    = $this->headers['User-Id'];
            $email      = $this->headers['Email'];

            $new_pwd    = $this->input->post('new_pwd');
            $old_pwd    = $this->input->post('old_pwd');

            $profile    = $this->m_global->get_data_all('user', null, ['user_id' => $user_id])[0];
            if($profile->user_password == md5_mod($old_pwd, $email)){
                $data['user_password']  = md5_mod($new_pwd, $email);
                $result                 = $this->m_global->update('user', $data, ['user_id' => $user_id]);
                if($result){
                    echo response_builder(true, 201);
                }else{
                    echo response_builder(false, 406, null, 'failed update data');
                }
            }else{
                echo response_builder(false, 412, null, 'wrong password');
            }

        }else{
            echo response_builder(false, 900);
        }
    }

    function search_user(){
        if($this->api_version == '1'){
            $user_id    = $this->headers['User-Id'];
            $query      = $this->input->post('query');
            $gender     = $this->input->post('gender');
            $clan       = $this->input->post('clan');
            $age        = explode('-', $this->input->post('age'));

            $join_profile       = [
                                    ['table' => 'user_father uf', 'on' => 'uf.userfather_user_id=user_id', 'tipe' => 'LEFT'],
                                    ['table' => 'clan ucf', 'on' => 'uf.userfather_clan_id=ucf.clan_id', 'tipe' => 'LEFT']
                                ];

            $where["CONCAT_WS(' ', user_firstname , user_lastname ) LIKE "] = '%'.$query.'%';
            $where['user_status']   = '1';

            if($clan != ""){
                $where['ucf.clan_id']            = $clan; 
            }

            if($gender != ""){
                $where['user_gender']            = $gender; 
            }

            $profile    = $this->m_global->get_data_all('user', $join_profile, $where, "CONCAT_WS(' ', user_firstname , user_lastname ) as user_fullname, user_avatar, user_id, user_gender, ucf.clan_name as father_clan, user_username, user_birthdate");
            
            $data       = [];
            for ($i=0; $i < count($profile); $i++) { 
                if($profile[$i]->user_id != $user_id){
                    $user_age                        = (String)get_age($profile[$i]->user_birthdate);
                    if($user_age >= $age[0] && $user_age <= $age[1]){
                        $profile[$i]->age            = $user_age;
                        $profile[$i]->father_clan    = ucfirst(strtolower($profile[$i]->father_clan));
                        $data[]                      = $profile[$i];
                    }
                }
            }

            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function check_clan($name, $user_id){
        $name   = strtoupper($name);
        $check  = $this->m_global->get_data_all('clan', null, ['clan_name' => $name]);
        if(count($check) > 0){
            return $check[0]->clan_id;
        }else{
            $data['clan_name']          = $name;
            $data['clan_created_by']    = $user_id;
            $data['clan_createddate']   = date('Y-m-d H:i:s');
            $result = $this->m_global->insert('clan', $data);
            if($result['status']){
                return $result['id'];
            }else{
                return '1';
            }
        }
    }

    function get_config(){
        if($this->api_version == '1'){
            $select         = $this->input->post('select');
            $result         = $this->m_global->get_data_all('config', null, null, $select)[0];
            echo response_builder(true, 200, $result);
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_quiz(){
        if($this->api_version == '1'){   
            $user_id    = $this->headers['User-Id'];

            $check_data = $this->m_global->count_data_all('score', null,['score_user_id' => $user_id]);
            if($check_data > 0){
                echo response_builder(false, 403, null, 'user already input quiz');
                exit;
            }

            $result = $this->m_global->get_data_all('question', null, ['question_status' => '1'], 'question_id, question_text');
            for ($i=0; $i < count($result); $i++) {
                $result[$i]->answer = $this->m_global->get_data_all('answer', null, ['answer_question_id' => $result[$i]->question_id, 'answer_status' => '1'], 'answer_id, answer_value, answer_text', null, ['answer_value', 'ASC']);
            }
            shuffle($result);
            echo response_builder(true, 200, $result);
        }else{
            echo response_builder(false, 900);
        }
    }

    function post_score_quiz(){
        if($this->api_version == '1'){          
            $data['score_user_id']      = $this->headers['User-Id'];
            $data['score_value']        = $this->input->post('score');
            $data['score_category']     = $this->input->post('category');
            $data['score_createddate']  = date('Y-m-d H:i:s');

            $result                     = $this->m_global->insert('score', $data);
            if($result['status']){
                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed insert data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

}

/* End of file config.php */
/* banner: ./application/modules/config/controllers/config.php */