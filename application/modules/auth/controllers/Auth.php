<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->headers          = $this->input->request_headers();
        $this->api_version      = $this->headers['Api-Version'];
        date_default_timezone_set('UTC');
        header('Content-Type: application/json');
    }

    public function index(){   
        // echo "Dolan Auth Rest API";
        echo md5_mod('apa', 'raflybejo@gmail.com');
    }

    function post_login_buyer(){
        if($this->api_version == '1'){
            $credential     = $this->input->post('credential');
            $type           = $this->input->post('type');
            $check_data     = $this->m_global->get_data_all('buyers', null, ['buyer_email' => $credential]);
            if(!empty($check_data)){
                $password       = md5_mod($this->input->post('password'), $check_data[0]->buyer_email);
                if($check_data[0]->buyer_password == $password || $type == 'socmed'){
                    if($check_data[0]->buyer_status == '1'){
                        $token       = $this->generate_file_token($check_data[0]->buyer_id, $check_data[0]->buyer_email);
                        $this->save_fcm_token($check_data[0]->buyer_id, '1');

                        $res_data = (Object) [
                            'id' => $check_data[0]->buyer_id,
                            'name' => $check_data[0]->buyer_name,
                            'email' => $check_data[0]->buyer_email,
                            'phone' => $check_data[0]->buyer_phone,
                            'address' => $check_data[0]->buyer_address,
                            'token' => $token
                        ];

                        echo response_builder(true, 200, $res_data);
                    }else{
                        echo response_builder(false, 423, null, ($check_data[0]->buyer_status == '0' ? 'buyer inactive' : 'buyer deleted'));
                    }
                }else{
                    echo response_builder(false, 412, null, 'password wrong');
                }
            }else{
                echo response_builder(false, 403, null, 'buyer not found');
            }
        }else{
            echo response_builder(false, 900);
        }
        
    }

    function generate_file_token($id,$email){
        $fcm_token              = $this->headers['Fcm-Token'];
        $model_name             = $this->headers['Model-Name'];
        $salt                   = $fcm_token.'%'.$model_name;
        $token                  = md5_mod($salt, $id.'%'.$email);
        $data['Api-Token']      = $token; 
        $data['Fcm-Token']      = $fcm_token;
        $data['User-Id']        = $id; 
        $data['Email']          = $email; 
        $data['Created']        = date('Y-m-d H:i:s'); 

        file_put_contents(URL_TOKENS.$token.'.json',json_encode($data));
        return $token;
    }

    function save_fcm_token($buyer_id, $type){
        $token                  = $this->headers['Fcm-Token'];
        $model_name             = $this->headers['Model-Name'];
        $app_version            = $this->headers['App-Version'];
        $os_version             = $this->headers['Os-Version'];
        $platform               = $this->headers['Platform'];
        $salt                   = $model_name.'-'.$app_version.'-'.$os_version.'-'.$platform;
        $device_uuid            = md5_mod($buyer_id, $salt);

        $data['device_fcm_token']  = $token;   
        $data['device_status']     = '1';   

        $check_token = $this->m_global->get_data_all('devices', null, ['device_user_id' => $buyer_id, 'device_user_type' => $type,'device_uuid' => $device_uuid]);
        if(count($check_token) == 0){
            $data['device_user_id']        = $buyer_id;
            $data['device_user_type']      = $type;
            $data['device_platform']       = $platform;
            $data['device_uuid']           = $device_uuid;
            $data['device_app_version']    = $app_version;
            $data['device_os']             = $os_version;
            $data['device_model']          = $model_name;
            $data['device_createddate']    = date('Y-m-d H:i:s');
            $result = $this->m_global->insert('devices', $data);
        }else{
            $result = $this->m_global->update('devices', $data, ['device_user_id' => $buyer_id, 'device_user_type' => $type, 'device_uuid' => $device_uuid]);
        }
    }

    // function disable_fcm_token(){
    //     if($this->api_version == '1'){
    //         $token      = $this->headers['Fcm-Token'];
    //         $buyer_id    = $this->headers['User-Id'];

    //         $data['fcm_status']   = '0';

    //         $result     = $this->m_global->update('fcm', $data, ['fcm_buyer_id' => $buyer_id, 'fcm_token' => $token]);
    //         if($result){
    //             echo response_builder(true, 201);
    //         }else{
    //             echo response_builder(false, 406, null, 'failed update data');
    //         }
    //     }else{
    //         echo response_builder(false, 900);
    //     }
    // }

    function post_register_buyer(){
        if($this->api_version == '1'){
            $email          = $this->input->post('email');
            $name           = $this->input->post('name');
            $password       = md5_mod($this->input->post('password'), $email);
            $phone          = $this->input->post('phone');

            $check_email    = $this->m_global->count_data_all('buyers',null,['buyer_email' => $email]);
            if($check_email > 0){
                echo response_builder(false, 403, null, 'email already used');
                exit;
            }

            $data['buyer_name']          = $name;
            $data['buyer_password']      = $password;
            $data['buyer_email']         = $email;
            $data['buyer_phone']         = $phone;
            $data['buyer_status']        = '1';
            $data['buyer_createddate']   = date('Y-m-d H:i:s');

            $result                     = $this->m_global->insert('buyers', $data);
            if($result['status']){
                $buyer_id       = $result['id'];

                $token          = $this->generate_file_token($buyer_id, $email);
                $res_data       = (Object) [
                                    'id' => $buyer_id,
                                    'name' => $name,
                                    'email' => $email,
                                    'phone' => $phone,
                                    'address' => '',
                                    'token' => $token
                                    ];

                $this->save_fcm_token($buyer_id, '1');
                echo response_builder(true, 201, $res_data);
            }else{
                echo response_builder(false, 406, null, 'failed create data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function post_register_seller(){
        if($this->api_version == '1'){
            $email          = $this->input->post('email');
            $name           = $this->input->post('name');
            $password       = md5_mod($this->input->post('password'), $email);
            $phone          = $this->input->post('phone');
            $phone          = $this->input->post('phone');
            $phone          = $this->input->post('phone');
            $phone          = $this->input->post('phone');
            $phone          = $this->input->post('phone');
            $phone          = $this->input->post('phone');
            $phone          = $this->input->post('phone');
            $phone          = $this->input->post('phone');
            $phone          = $this->input->post('phone');
            $phone          = $this->input->post('phone');
            $phone          = $this->input->post('phone');
            $phone          = $this->input->post('phone');
            $phone          = $this->input->post('phone');
            $phone          = $this->input->post('phone');

            $check_email    = $this->m_global->count_data_all('sellers',null,['buyer_email' => $email]);
            if($check_email > 0){
                echo response_builder(false, 403, null, 'email already used');
                exit;
            }

            $data['buyer_name']          = $name;
            $data['buyer_password']      = $password;
            $data['buyer_email']         = $email;
            $data['buyer_phone']         = $phone;
            $data['buyer_status']        = '1';
            $data['buyer_createddate']   = date('Y-m-d H:i:s');

            $result                     = $this->m_global->insert('sellers', $data);
            if($result['status']){
                $buyer_id       = $result['id'];

                $token          = $this->generate_file_token($buyer_id, $email);
                $res_data       = (Object) [
                                    'id' => $buyer_id,
                                    'name' => $name,
                                    'email' => $email,
                                    'phone' => $phone,
                                    'address' => '',
                                    'token' => $token
                                    ];

                $this->save_fcm_token($buyer_id, '1');
                echo response_builder(true, 201, $res_data);
            }else{
                echo response_builder(false, 406, null, 'failed create data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

}

/* End of file config.php */
/* banner: ./application/modules/config/controllers/config.php */