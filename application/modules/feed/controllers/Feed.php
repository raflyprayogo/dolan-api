<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feed extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->headers          = $this->input->request_headers();
        $this->api_version      = $this->headers['Api-Version'];
        date_default_timezone_set('UTC');
        header('Content-Type: application/json');
        acc_token();
    }

    public function index(){   
        echo "Rokkap Feed Rest API";
    }

    function get_timeline(){
        if($this->api_version == '1'){
            $user_id    = $this->headers['User-Id'];
            $target_id  = $this->input->post('target_id');
            $join   = [
                        ['table' => 'user', 'on' => 'user_id=feed_user_id']
                    ];
            $select = "CONCAT_WS(' ', user_firstname , user_lastname ) as user_fullname,
                            user_avatar,
                            user_id,
                            (select count(*) from feed_comment where feedcom_feed_id = feed_id and feedcom_status = '1') as total_comment,
                            (select count(*) from feed_like where feedlike_feed_id = feed_id) as total_like,
                            (select count(*) from feed_like where feedlike_feed_id = feed_id and feedlike_user_id = $user_id) as is_like,
                            feed_id,
                            feed_content,
                            feed_lat,
                            feed_long,
                            feed_createddate
                        ";

            $arr_friend         = []; 
            $join_friend        = [
                                    ['table' => 'user', 'on' => 'friend_target_id=user_id']
                                ];
            $data_friend        = $this->m_global->get_data_all('friend', $join_friend, ['friend_user_id' => $user_id, 'friend_status' => '2'], "user_id");
            for ($i=0; $i < count($data_friend); $i++) { 
                $arr_friend[$i] = $data_friend[$i]->user_id; 
            }

            $data   = $this->m_global->get_data_all('feed', $join, ($target_id == '0' ? ['feed_status' => '1'] : ['feed_status' => '1', 'feed_user_id' => $target_id] ) , $select, null, ['feed_createddate', 'DESC']);
            $response               = [];
            for ($i=0; $i < count($data); $i++) { 
                $get_photos         = $this->m_global->get_data_all('feed_image', null, ['feedimage_feed_id' => $data[$i]->feed_id, 'feedimage_status' => '1'], 'feedimage_link');
                $data_photos        = [];

                for ($p=0; $p < count($get_photos); $p++) { 
                    $data_photos[]  = $get_photos[$p]->feedimage_link;
                }

                $data[$i]->is_like  = ($data[$i]->is_like == '1' ? true : false);
                $data[$i]->photos   = $data_photos;

                if(in_array($data[$i]->user_id, $arr_friend) || $data[$i]->user_id == $user_id){
                    $response[]     = $data[$i];
                }
            }

            echo response_builder(true, 200, $response);
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_single_timeline(){
        if($this->api_version == '1'){
            $user_id        = $this->headers['User-Id'];
            $feed_id        = $this->input->post('feed_id');
            $join   = [
                        ['table' => 'user', 'on' => 'user_id=feed_user_id']
                    ];
            $select = "CONCAT_WS(' ', user_firstname , user_lastname ) as user_fullname,
                            user_avatar,
                            user_id,
                            (select count(*) from feed_comment where feedcom_feed_id = feed_id and feedcom_status = '1') as total_comment,
                            (select count(*) from feed_like where feedlike_feed_id = feed_id) as total_like,
                            (select count(*) from feed_like where feedlike_feed_id = feed_id and feedlike_user_id = $user_id) as is_like,
                            feed_id,
                            feed_content,
                            feed_lat,
                            feed_long,
                            feed_createddate
                        ";

            $data   = $this->m_global->get_data_all('feed', $join, ['feed_id' => $feed_id], $select);
            for ($i=0; $i < count($data); $i++) { 
                $get_photos         = $this->m_global->get_data_all('feed_image', null, ['feedimage_feed_id' => $data[$i]->feed_id, 'feedimage_status' => '1'], 'feedimage_link');
                $data_photos        = [];

                for ($p=0; $p < count($get_photos); $p++) { 
                    $data_photos[]  = $get_photos[$p]->feedimage_link;
                }

                $data[$i]->is_like  = ($data[$i]->is_like == '1' ? true : false);
                $data[$i]->photos   = $data_photos;
            }
            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function post_like(){
        if($this->api_version == '1'){
            $owner_id   = $this->input->post('owner_id');
            $feed_id    = $this->input->post('feed_id');
            $user_id    = $this->headers['User-Id'];
            $user_name  = $this->headers['User-Name'];
            
            $data['feedlike_feed_id']        = $feed_id;
            $data['feedlike_user_id']        = $user_id;
            $data['feedlike_createddate']    = date('Y-m-d H:i:s'); 

            $result     = $this->m_global->insert('feed_like', $data);
            if($result['status']){
                if($owner_id != $user_id){
                    insert_notif_list($owner_id, $user_id, 'like', $feed_id);
                    send_notification($owner_id, $user_name,'Menyukai status anda', 'like', '{"feed_id":'.$feed_id.'}');
                }
                
                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed create data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_liked_user(){
        if($this->api_version == '1'){
            $feed_id    = $this->input->post('feed_id');
            $user_id    = $this->headers['User-Id'];

            $join       = [['table' => 'user', 'on' => 'feedlike_user_id=user_id']];
            $data       = $this->m_global->get_data_all('feed_like', $join, ['feedlike_feed_id' => $feed_id], "CONCAT_WS(' ', user_firstname , user_lastname ) as user_fullname,user_avatar,user_id", null, ['feedlike_createddate', 'ASC']);

            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_comment(){
        if($this->api_version == '1'){
            $feed_id    = $this->input->post('feed_id');
            $join   = [
                        ['table' => 'user', 'on' => 'user_id=feedcom_user_id']
                    ];
            $select = "CONCAT_WS(' ', user_firstname , user_lastname ) as user_fullname,
                            user_avatar,
                            user_id,
                            feedcom_id,
                            feedcom_content,
                            feedcom_createddate
                        ";    
            $data   = $this->m_global->get_data_all('feed_comment', $join, ['feedcom_feed_id' => $feed_id, 'feedcom_status' => '1'], $select, null, ['feedcom_createddate', 'ASC']);
            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function post_feed(){
        if($this->api_version == '1'){
            $lat            = $this->input->post('lat');
            $long           = $this->input->post('long');
            $image_link     = $this->input->post('image_link');
            $content_text   = $this->input->post('content_text'); 
            $user_id        = $this->headers['User-Id'];
            
            $data['feed_content']           = $content_text;
            $data['feed_user_id']           = $user_id;
            $data['feed_lat']               = $lat;
            $data['feed_long']              = $long;
            $data['feed_createddate']       = date('Y-m-d H:i:s'); 

            $result     = $this->m_global->insert('feed', $data);
            if($result['status']){

                if($image_link != ''){
                    $items = json_decode($image_link, true);
                    $succ  = [];
                    for ($i=0; $i < count($items); $i++) { 
                        $dimg['feedimage_link']        = $items[$i]['data'];
                        $dimg['feedimage_feed_id']     = $result['id'];
                        $result_d = $this->m_global->insert('feed_image', $dimg);
                        if($result_d['status']){
                            $succ[] = $result_d['id'];
                        }
                    }
                }

                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed create data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function post_comment(){
        if($this->api_version == '1'){
            $feed_id    = $this->input->post('feed_id');
            $comment    = $this->input->post('comment'); 
            $user_id    = $this->headers['User-Id'];
            $user_name  = $this->headers['User-Name'];
            
            $data['feedcom_feed_id']        = $feed_id;
            $data['feedcom_user_id']        = $user_id;
            $data['feedcom_content']        = $comment;
            $data['feedcom_createddate']    = date('Y-m-d H:i:s'); 

            $result     = $this->m_global->insert('feed_comment', $data);
            if($result['status']){

                $owner_id       = $this->m_global->get_data_all('feed', null, ['feed_id' => $feed_id], 'feed_user_id')[0]->feed_user_id;
                $targets_id     = [];
                if($user_id != $owner_id){
                    $targets_id[] = $owner_id;
                }

                $subscriber     = $this->m_global->get_data_all('feed_comment', null, ['feedcom_feed_id' => $feed_id], 'feedcom_user_id');

                for ($i=0; $i < count($subscriber); $i++) { 
                    $s_id       = $subscriber[$i]->feedcom_user_id;
                    if($user_id != $s_id){
                        if(!in_array($s_id, $targets_id)){
                            $targets_id[]   = $s_id;
                        }
                    }                
                }

                for ($i=0; $i < count($targets_id); $i++) { 
                    insert_notif_list($targets_id[$i], $user_id, 'comment', $result['id']);
                    send_notification($targets_id[$i], $user_name,"Berkomentar '".$comment."'", 'comment', '{"feed_id":'.$feed_id.'}');
                }

                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed create data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_photos(){
        if($this->api_version == '1'){
            $user_id    = $this->input->post('user_id');
            $select     = "feed_content_image,feed_createddate";    
            $data       = $this->m_global->get_data_all('feed', null, ['feed_user_id' => $user_id , 'feed_status' => '1', 'feed_content_image <>' => ''], $select, null, ['feed_createddate', 'DESC']);
            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function delete_feed(){
        if($this->api_version == '1'){
            $feed_id    = $this->input->post('feed_id');

            $data['feed_status']    = '99';

            $result     = $this->m_global->update('feed', $data, ['feed_id' => $feed_id]);
            if($result){
                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed delete feed');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function delete_comment(){
        if($this->api_version == '1'){
            $comment_id    = $this->input->post('comment_id');

            $data['feedcom_status']    = '99';

            $result     = $this->m_global->update('feed_comment', $data, ['feedcom_id' => $comment_id]);
            if($result){
                echo response_builder(true, 201);
            }else{
                echo response_builder(false, 406, null, 'failed delete comment');
            }
        }else{
            echo response_builder(false, 900);
        }
    }










}

/* End of file config.php */
/* banner: ./application/modules/config/controllers/config.php */